
const ИМЯ = "Изображение|Image UIkit";

// // // //


УстановитьUIkit = function(мир)
{
    var модуль = мир.модули.модульПоИмени(ИМЯ);

    var css = модуль.содержимое["/uikit_3.2.0.min.css"];
    var js = модуль.содержимое["/uikit_3.2.0.min.js"];
    var iconsJS = модуль.содержимое["/uikit-icons_3.2.0.min.js"];

    // Применить стиль.
    var style = document.createElement("style");
    document.head.appendChild(style);
    style.innerHTML = css;

    function загрузитьСкрипт(код)
    {
        var скрипт = document.createElement("script");
        скрипт.innerHTML = код;
        document.body.appendChild(скрипт);
    }

    загрузитьСкрипт(js);
    загрузитьСкрипт(iconsJS);
};


// // // //


ОтобразитьUIkit = function(мир)
{
    var модуль = мир.модули.модульПоИмени(ИМЯ);
    var изображение = модуль.содержимое["/балкончик.jpg"];
    var изо64 = base64js.fromByteArray(new Uint8Array(изображение));

    document.body.innerHTML = `

<center class="uk-margin-left uk-margin-right uk-margin-top uk-margin-bottom">

<div class="uk-card uk-card-default uk-width-1-2@m uk-margin-large">
    <div class="uk-card-header uk-text-left">
        <h3 class="uk-card-title">Очистить кэш | Clear the cache</h3>
    </div>
    <div class="uk-card-body uk-text-left">
        <a href="http://gitjs.org?https://bitbucket.org/gitjs/ochistit-clear/raw/master/0000">Очистить | Clear</a>
    </div>
</div>

<div class="uk-card uk-card-default uk-width-1-2@m uk-margin-large">
    <div class="uk-card-header uk-text-left">
        <h3 class="uk-card-title">Что такое GitJS?</h3>
    </div>
    <div class="uk-card-body uk-text-left">
        GitJS - это попытка реализовать долговечные полезные приложения, которые можно использовать везде: на ПК, планшетах, мобилках.
    </div>
</div>

<div class="uk-card uk-card-default uk-width-1-2@m uk-margin-large">
    <div class="uk-card-header uk-text-left">
        <h3 class="uk-card-title">What is GitJS?</h3>
    </div>
    <div class="uk-card-body uk-text-left">
        GitJS is an attempt to implement durable useful applications that can be used anywere: desktop, tablet, mobile.
    </div>
</div>

<div class="uk-card uk-card-default uk-width-1-2@m uk-margin-large">
    <div class="uk-card-header">
        <h3 class="uk-card-title">Изображение | Image</h3>
    </div>
    <div class="uk-card-body">
        <img src="data:image/jpeg;base64,${изо64}">
    </div>
</div>

<div class="uk-card uk-card-default uk-width-1-2@m uk-margin-large">
    <div class="uk-card-header uk-text-left">
        <h3 class="uk-card-title">Как запустить свой модуль?</h3>
    </div>
    <div class="uk-card-body uk-text-left">
        Просто передайте путь до указателя (файл 0000) в адресной строке вот так: <a href="http://gitjs.org?https://bitbucket.org/gitjs/izobrazhenie-image-uikit/raw/master/0000">http://gitjs.org?https://bitbucket.org/gitjs/izobrazhenie-image-uikit/raw/master/0000</a>.
    </div>
</div>

<div class="uk-card uk-card-default uk-width-1-2@m uk-margin-large">
    <div class="uk-card-header uk-text-left">
        <h3 class="uk-card-title">How to run your module?</h3>
    </div>
    <div class="uk-card-body uk-text-left">
        Simply pass URL to a pointer (0000 file) in the address bar like that: <a href="http://gitjs.org?https://bitbucket.org/gitjs/izobrazhenie-image-uikit/raw/master/0000">http://gitjs.org?https://bitbucket.org/gitjs/izobrazhenie-image-uikit/raw/master/0000</a>.
    </div>
</div>

<div class="uk-card uk-card-default uk-width-1-2@m uk-margin-large">
    <div class="uk-card-header uk-text-left">
        <h3 class="uk-card-title">Примеры | Samples</h3>
    </div>
    <div class="uk-card-body uk-text-left">
        <p>По ссылке ниже разнообразные примеры | Lhe link below contains lots of samples</p>
        <p><a href="http://gitjs.org?https://bitbucket.org/gitjs/primery-samples/raw/master/0000">http://gitjs.org?https://bitbucket.org/gitjs/primery-samples/raw/master/0000</a></p>
    </div>
</div>

</center>

	`;
};

СообщитьОНеобходимостиЗапуститьЧерезПривет = function()
{
    document.body.innerHTML = `

    <p>Вы открыли модуль 'Изображение|Image UIkit' не через модуль 'Привет|Hello'. Перейдите по ссылке <a href="http://gitjs.org">http://gitjs.org</a></p>
    <p>You launched 'Изображение|Image UIkit' module outside of 'Привет|Hello' module. Go to <a href="http://gitjs.org">http://gitjs.org</a></p>

    `;
};
